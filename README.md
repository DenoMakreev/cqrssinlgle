This is a demo project on how to use CQRS and Event Store to keep different instances of the same object up to date with the correct state.
This concept can be used in a microservice architecture with multiple instances of the same service running.

In this demo both the Commands and Queries are executed from the same object.

This project uses a custom EventStore API that can be downloaded from 
https://gitlab.com/DenoMakreev/eventstorecustomapi