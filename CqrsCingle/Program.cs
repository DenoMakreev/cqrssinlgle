﻿using CqrsSingle.Events;
using EventStore.Core;
using EventStoreInterfaces;
using System;
using System.Threading;

namespace CqrsSingle
{
    class Program
    {
        /*  
            In the example you can see that we're creating 2 instances of the same object that connect to the same Event stream in event store.
            They can write events in the store individually (that is the Command part of the pattern)
            Both of them aim to update their model from the stream (that is the Query part) so we end up with 2 different objects that keep their internal models 
            identical based on the queries they make to the stream (in this case the query is a catchup subscription to the event stream).
            We only update our model on a read!!! 
            
            Note: In order to keep consistency trough restarts you might change the way your queries are implemented for example a persistent subscription to EventStore stream
        */

        static void Main(string[] args)
        {
            var streamId = Guid.NewGuid();

            //Initializing the EventStore repo and subscriber
            IEventStoreRepo esRepo = new EventStoreRepo();
            IEventStoreSubscriptionHandler esSub = new EventStoreSubscriptionHandler();

            //Creating 2 different instances of the same object
            var Actor1 = new Actor(esRepo, streamId, esSub);
            var Actor2 = new Actor(esRepo, streamId, esSub);


            var foo = new Foo() { FooOutput = "FooEvent" };
            var bar = new Bar() { BarOutput = "BarEvent" };

            //We subscribe from 0 so we see events from event 1 on. First event in stream is aways invisible to a catchup subscription
            Actor1.WriteEvent(foo);

            //Writing 2 events in the stream each one from a different instance
            Actor1.WriteEvent(foo);
            Actor2.WriteEvent(bar);

            Console.ReadKey();

            //Checking the state of each instance to confirm they are the same
            Console.WriteLine($"Actor1 counters {Actor1.GetState()}");
            Console.WriteLine($"Actor2 counters {Actor2.GetState()}");

            Console.ReadKey();
        }
    }
}
