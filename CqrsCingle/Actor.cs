﻿using CqrsSingle.Events;
using EventStore.ClientAPI;
using EventStoreInterfaces;
using Newtonsoft.Json;
using System;
using System.Text;

namespace CqrsSingle
{
    public class Actor
    {

        private IEventStoreRepo _repo;
        private IEventStoreSubscriptionHandler _subscriber;
        private Guid _streamId;
        private Guid _internalid;
        private string _streamName;

        private int _fooCounter;
        private int _barCounter;


        public Actor(IEventStoreRepo repo, Guid streamId, IEventStoreSubscriptionHandler subscriber)
        {
            _internalid = Guid.NewGuid();
            _repo = repo;
            _subscriber = subscriber;
            _streamId = streamId;
            _streamName = CreateStreamName();

            //connecting to event store and starting subscription to the event stream
            ConnectToEventStore();
            SubsribeToStream();
        }

        private void ConnectToEventStore()
        {
            _repo.CreateConnection();
            _subscriber.CreateConnection();
        }

        private string CreateStreamName()
        {
            string streamName = _repo.CreateStreamName(this.GetType().ToString(), _streamId);

            return streamName;
        }

        public void WriteEvent(object evt)
        {
            _repo.WriteToStream(_streamName, evt);
        }

        private void SubsribeToStream()
        {
            _subscriber.SubscribeToStream(_streamName, HandleEventAppeeared);
        }

        public Tuple<int,int> GetState()
        {
            var result = new Tuple<int, int>(_fooCounter, _barCounter);
            return result;
        }

        private void HandleEventAppeeared(EventStoreCatchUpSubscription e, ResolvedEvent evt)
        {
            var esJsonData = Encoding.UTF8.GetString(evt.Event.Data);

            if (evt.Event.EventType == "Foo")
            {
                var objState = JsonConvert.DeserializeObject<Foo>(esJsonData);

                _fooCounter++;
                Console.WriteLine($"Foo detected in instance {_internalid}: {objState.FooOutput} Updating state");
            }

            if (evt.Event.EventType == "Bar")
            {
                var objState = JsonConvert.DeserializeObject<Bar>(esJsonData);

                _barCounter++;
                Console.WriteLine($"Bar detected in instance {_internalid}:: {objState.BarOutput}  Updating state");
            }
        }
    }
}
